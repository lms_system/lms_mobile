import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:lms/src/features/presentation/pages/home/pages/main_page.dart';

class AuthServices {
  final String loginBaseUrl =
      "https://lms-back.nvrbckdown.uz/lms/api/v1/auth/login";
  final Dio dio = Dio();

  Future<void> login(
      BuildContext context, String email, String password) async {
    final Map<String, String> credentials = {
      'email': email,
      'password': password,
    };
    try {
      final response = await dio.post(
        loginBaseUrl,
        data: credentials,
        options: Options(
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
        ),
      );
      if (response.statusCode == 200) {
        print("Success!");
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainPage()));
      } else {
        print("Failed to login: ${response.statusCode}");
      }
    } on DioError catch (e) {
      print("Error: ${e.message}");
    } catch (e) {
      print("Error: $e");
    }
  }
}
