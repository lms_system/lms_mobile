import 'package:flutter/material.dart';
import 'package:lms/src/features/presentation/pages/auth_pages/auth_service.dart';
import 'package:lms/src/features/presentation/pages/utils/color.dart';
import 'package:lms/src/features/presentation/pages/utils/text.dart';
import 'package:lms/src/features/presentation/pages/home/email_input_widget.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({Key? key}) : super(key: key);

  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final AuthServices _authServices = AuthServices();
  bool isChecked = false;
  @override
  Widget build(
    BuildContext context,
  ) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.only(top: 80, left: 15),
              child: Text(
                TextItem.loginTitle,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w700,
                  color: ColorItem.purple,
                ),
              ),
            ),
            const SizedBox(height: 150),
            const Padding(
              padding: EdgeInsets.only(left: 25),
              child: Text(
                TextItem.introductorytext,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.w600),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(left: 25),
              child: Text(
                TextItem.inputTextEmail,
                style: TextStyle(fontSize: 16),
              ),
            ),
            EmailInputWidget(
              emailController: emailController,
              passwordController: passwordController,
            ),
            const SizedBox(height: 10),
            const SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 25),
                  child: Checkbox(
                    value: isChecked,
                    activeColor: ColorItem.purple,
                    onChanged: (newBool) {
                      setState(() {
                        isChecked = newBool!;
                      });
                    },
                  ),
                ),
                const Text(
                  TextItem.remember,
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                ),
                const Spacer(),
                TextButton(
                  onPressed: () {},
                  child: const Text(
                    TextItem.forgotPassordText,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 14,
                      color: ColorItem.purple,
                    ),
                  ),
                )
              ],
            ),
            const SizedBox(height: 170),
            Padding(
              padding: const EdgeInsets.only(left: 25, right: 12),
              child: MaterialButton(
                minWidth: 399,
                height: 45,
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    _authServices.login(
                      context,
                      emailController.text,
                      passwordController.text,
                    );
                  }
                },
                color: ColorItem.purple,
                elevation: 9,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                child: const Text(
                  TextItem.enter,
                  style: TextStyle(
                    color: ColorItem.whiteContainer,
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
