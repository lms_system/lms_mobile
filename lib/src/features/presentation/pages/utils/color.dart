import 'dart:ui';

import 'package:flutter/material.dart';

class ColorItem {
  static const Color purple = Colors.deepPurple;
  static const Color red = Color.fromARGB(255, 236, 28, 28);
  static const Color whiteContainer = Colors.white;
  static const Color black = Colors.black;
  static const Color blue = Colors.blue;
}
