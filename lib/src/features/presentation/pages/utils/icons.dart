class AllIcons {
  static const String commentary = "assets/icons/Message_write.svg";
  static const String bell = "assets/icons/union.svg";
  static const String back = "assets/icons/chevron_left.svg";
  static const String vector = "assets/icons/Vector.svg";
}
