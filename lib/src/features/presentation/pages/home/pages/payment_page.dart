import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:lms/src/features/presentation/pages/utils/color.dart';
import 'package:lms/src/features/presentation/pages/utils/text.dart';
import 'package:lms/src/features/presentation/pages/home/course.dart'; // Import the Course widget

class PaymentPage extends StatelessWidget {
  const PaymentPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(CupertinoIcons.back),
            onPressed: () {},
          ),
          title: const Row(
            children: [
              SizedBox(width: 105),
              Text(
                TextItem.payment,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                  fontFamily: "Inter",
                ),
              ),
            ],
          ),
          bottom: const TabBar(
            unselectedLabelColor: ColorItem.black,
            indicatorPadding: EdgeInsets.symmetric(horizontal: 10),
            tabs: [
              Align(
                child: Text(
                  TextItem.all,
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
              ),
              Align(
                child: Text(
                  TextItem.indebtedness,
                  style: TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                ),
              )
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Course(isPaid: false),
                  Course(isPaid: true,),
                ],
              ),
            ),
            SingleChildScrollView(
              child: Column(
                children: [
                  Course(isPaid: true,),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
