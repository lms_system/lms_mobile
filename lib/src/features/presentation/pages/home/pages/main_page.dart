import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lms/src/features/presentation/pages/utils/color.dart';
import 'package:lms/src/features/presentation/pages/utils/icons.dart';
import 'package:lms/src/features/presentation/pages/utils/images.dart';
import 'package:lms/src/features/presentation/pages/utils/text.dart';

class MainPage extends StatelessWidget {
  const MainPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(70),
        child: ClipRRect(
          borderRadius: const BorderRadius.vertical(
            bottom: Radius.circular(30),
          ),
          child: AppBar(
            automaticallyImplyLeading: false,
            backgroundColor: ColorItem.purple,
            actions: [
              Row(
                children: [
                  const CircleAvatar(
                    radius: 20,
                    backgroundImage: AssetImage(Assets.circleAvatar),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  const Text(
                    TextItem.person,
                    style: TextStyle(fontWeight: FontWeight.w500, fontSize: 20),
                  ),
                  const SizedBox(
                    width: 80,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      color: ColorItem.whiteContainer,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      icon: SvgPicture.asset(AllIcons.bell),
                      onPressed: () {},
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Container(
                    decoration: const BoxDecoration(
                      color: ColorItem.whiteContainer,
                      shape: BoxShape.circle,
                    ),
                    child: IconButton(
                      icon: SvgPicture.asset(AllIcons.commentary),
                      onPressed: () {},
                    ),
                  ),
                  const SizedBox(width: 30),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
