import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmailInputWidget extends StatefulWidget {
  const EmailInputWidget({
    Key? key,
    required this.emailController,
    required this.passwordController,
  }) : super(key: key);

  final TextEditingController emailController;
  final TextEditingController passwordController;

  @override
  _EmailInputWidgetState createState() => _EmailInputWidgetState();
}

class _EmailInputWidgetState extends State<EmailInputWidget> {
  bool isIconVisible = false;
  bool hidePassword = true;
  bool isChecked = false;
RegExp passwordRegExp= RegExp(    r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#$%^&*()\-_=+{}|;:,<.>/?]).{8,}$');

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 399,
          height: 48,
          child: TextFormField(
            showCursor: true,
            controller: widget.emailController,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              suffixIcon: const Icon(CupertinoIcons.person),
              hintStyle: const TextStyle(color: Colors.blueGrey),
            ),
            validator: (value) {
              if (value!.isEmpty ||
                  !RegExp(r'^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$')
                      .hasMatch(value)) {
                return "Email can't be empty or invalid";
              }
              return null;
            },
          ),
        ),
        const SizedBox(height: 10),
        const Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 30),
              child: Text(
                "Parol",
                style: TextStyle(fontSize: 16),
              ),
            ),
          ],
        ),
        Container(
          width: 399,
          height: 48,
          child: TextFormField(
            onChanged: (value) {
              value.isNotEmpty
                  ? setState(() => isIconVisible = true)
                  : setState(() => isIconVisible = false);
              setState(() => value.isNotEmpty
                  ? isIconVisible = true
                  : isIconVisible = false);
            },
            obscureText: hidePassword,
            controller: widget.passwordController,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(12),
              ),
              suffixIcon: isIconVisible
                  ? IconButton(
                      onPressed: () {
                        setState(() => hidePassword = !hidePassword);
                      },
                      icon: Icon(
                        hidePassword ? Icons.visibility_off : Icons.visibility,
                      ),
                    )
                  : null,
            ),
            validator: (value) {
              if (value!.isEmpty || !passwordRegExp.hasMatch(value)) {
                  return "Password must contain at least 8 characters, including at least one uppercase letter, one lowercase letter, one number, and one special character";
              }
              return null;
            },
          ),
        ),
      ],
    );
  }
}
