import 'package:flutter/material.dart';
import 'package:lms/src/features/presentation/pages/utils/text.dart';

class BottomNavBar extends StatelessWidget {
  final int currentIndex;
  final Function(int) onTap;

  const BottomNavBar({
    Key? key,
    required this.currentIndex,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home_outlined),
          label: TextItem.home,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.school),
          label: TextItem.course,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.payment),
          label: TextItem.navbarPayment,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.calendar_today),
          label: TextItem.table,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person_outline),
          label: TextItem.account,
        ),
      ],
      currentIndex: currentIndex,
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.grey,
      onTap: onTap,
    );
  }
}
