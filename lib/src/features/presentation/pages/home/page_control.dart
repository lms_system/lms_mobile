import 'package:flutter/material.dart';
import 'package:lms/src/features/presentation/pages/home/pages/account_page.dart';
import 'package:lms/src/features/presentation/pages/home/pages/courses_page.dart';
import 'package:lms/src/features/presentation/pages/home/pages/main_page.dart';
import 'package:lms/src/features/presentation/pages/home/pages/payment_page.dart';
import 'package:lms/src/features/presentation/pages/home/pages/table_page.dart';
import 'package:lms/src/features/presentation/pages/home/bottom_nav_bar.dart';

class PageControl extends StatefulWidget {
  _PageControlState createState() => _PageControlState();
}

class _PageControlState extends State<PageControl> {
  int _selectedIndex = 0;
  late final PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController(initialPage: _selectedIndex);
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      _pageController.jumpToPage(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        controller: _pageController,
        onPageChanged: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
        children: <Widget>[
          MainPage(),
          CoursesPage(),
          PaymentPage(),
          TablePage(),
          AccountPage()
        ],
      ),
      bottomNavigationBar: BottomNavBar(
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
