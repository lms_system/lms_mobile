import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lms/src/features/presentation/pages/utils/images.dart';
import 'package:lms/src/features/presentation/pages/utils/text.dart';

class Course extends StatelessWidget {
  final bool isPaid; 

  const Course({Key? key, required this.isPaid}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.all(20),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Image.asset(
                  Assets.goLogo,
                  width: 96,
                  height: 64,
                ),
                const SizedBox(width: 50),
                const Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      TextItem.gotitle,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.blue,
                      ),
                    ),
                    Text(
                      TextItem.money,
                      style: TextStyle(fontSize: 20),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10),
            Column(
              children: [
                Row(
                  children: [
                    Column(
                      children: [
                        const Text(
                          TextItem.courseNumber,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        TextButton.icon(
                            onPressed: (){},
                            icon: const Icon(CupertinoIcons.clock,size: 11,),
                            label:const Text(TextItem.weekWithin,style: TextStyle(fontSize: 8,fontWeight: FontWeight.w400,color: Colors.black54),)
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: [
                        const Text(
                          TextItem.money,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        TextButton.icon(
                            onPressed: (){},
                            icon: const Icon(CupertinoIcons.clock,size: 11,),
                            label: isPaid ? const Text(TextItem.monthlyPaid,style: TextStyle(fontSize: 8,fontWeight: FontWeight.w400,color: Colors.black54,))
                                          : const Text(TextItem.monthlyPaid,style: TextStyle(fontSize: 8,fontWeight: FontWeight.w400,color: Colors.black54,))
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
            Column(
              children: [
                Row(
                  children: [
                    Column(
                      children: [
                        const Text(
                          TextItem.time,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        TextButton.icon(
                            onPressed: (){},
                            icon: const Icon(CupertinoIcons.clock,size: 11,),
                            label:const Text(TextItem.lesson,style: TextStyle(fontSize: 8,fontWeight: FontWeight.w400,color: Colors.black54),)
                        ),
                      ],
                    ),
                    Spacer(),
                    Column(
                      children: [
                        const Text(
                          TextItem.date,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        TextButton.icon(
                            onPressed: (){},
                            icon: const Icon(CupertinoIcons.clock,size: 11,),
                            label:const Text(TextItem.courseStart,style: TextStyle(fontSize: 8,fontWeight: FontWeight.w400,color: Colors.black54),)
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
            MaterialButton(
              minWidth: 322,
              height: 32,
              onPressed: () {},
              color: Colors.deepPurple,
              elevation: 9,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8),
              ),
              child: Text(
                isPaid ? TextItem.paid : TextItem.unpaid,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
