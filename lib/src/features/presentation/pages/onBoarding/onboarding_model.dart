class OnbordingContent {
  String image;
  String title;
  String description_1;
  String description_2;

  OnbordingContent({
    required this.image,
    required this.title,
    required this.description_1,
    required this.description_2,
  });
}

List<OnbordingContent>contents=[
  OnbordingContent(
    image:'assets/images/onboarding_1.png',
    title: "Farzandingiz akademiklarda qanday ishlashini ko'ring",
    description_1:"Farzandingizning ish faoliyatini solishtirish ",
    description_2:"imkonini beruvchi imtihon baholarini oʻrganing",
  ),
  OnbordingContent(
    image: 'assets/images/onboarding_2.png',
    title: "Farzandingiz akademiklarda qanday ishlashini ko'ring",
    description_1: "Farzandingizning ish faoliyatini solishtirish",
    description_2: " imkonini beruvchi imtihon baholarini oʻrganing",
  ),
  OnbordingContent(
    image: 'assets/images/onboarding_3.png',
    title: "Farzandingiz akademiklarda qanday ishlashini ko'ring",
    description_1: "Farzandingizning ish faoliyatini solishtirish",
    description_2: "imkonini beruvchi imtihon baholarini oʻrganing",
  )
];
