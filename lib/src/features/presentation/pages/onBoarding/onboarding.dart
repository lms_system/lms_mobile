import 'package:flutter/material.dart';
import 'package:lms/src/features/presentation/pages/auth_pages/signIn_screen.dart';
import 'package:lms/src/features/presentation/pages/onBoarding/onboarding_model.dart';
import 'package:lms/src/features/presentation/pages/home/page_control.dart';
// import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class Onboarding extends StatefulWidget {
  const Onboarding({super.key});

  @override
  State<Onboarding> createState() => _OnboardingState();
}

class _OnboardingState extends State<Onboarding> {
  int currentIndex = 0;
  late PageController _controller;

  @override
  void initState() {
    _controller = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 300, top: 50),
            child: GestureDetector(
              child: const Text(
                "O'tkazish",
                style: TextStyle(
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w600,
                  fontSize: 16,
                  color: Color(0xff0372B7),
                ),
              ),
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SignInScreen()));
              },
            ),
          ),
          Expanded(
            child: PageView.builder(
                controller: _controller,
                onPageChanged: (int index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
                itemCount: contents.length,
                itemBuilder: (_, i) {
                  return Column(
                    children: [
                      const SizedBox(height: 100),
                      Column(
                        children: [
                          Image.asset(
                            contents[i].image,
                            height: 310,
                          ),
                          SizedBox(
                            width: 300,
                            height: 60,
                            child: Text(contents[i].title,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontFamily: 'Inter',
                                  fontSize: 20,
                                  fontWeight: FontWeight.w600,
                                )),
                          ),
                          SizedBox(
                            width: 400,
                            height: 20,
                            child: Text(contents[i].description_1,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontFamily: 'Inter',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: Color(0xff000000),
                                )),
                          ),
                          SizedBox(
                            width: 400,
                            height: 20,
                            child: Text(contents[i].description_2,
                                textAlign: TextAlign.center,
                                style: const TextStyle(
                                  fontFamily: 'Inter',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,
                                  color: Color(0xff000000),
                                )),
                          ),
                        ],
                      ),
                    ],
                  );
                }),
          ),
          // GestureDetector(
          //   onTap: () {
          //     setState(() {
          //       currentIndex = (currentIndex + 1) % contents.length;
          //     });
          //   },
          //   child: Padding(
          //     padding: const EdgeInsets.only(bottom: 115),
          //     child: AnimatedSmoothIndicator(
          //       activeIndex: currentIndex,
          //       count: contents.length,
          //       effect: const WormEffect(
          //           activeDotColor: Color(0xff0372B7),
          //           dotHeight: 5,
          //           dotWidth: 25),
          //     ),
          //   ),
          // ),
          Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child: GestureDetector(
                child: Container(
                    width: 343,
                    height: 60,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: const Color(0xff0072b9)),
                    child: const Center(
                        child: Text(
                      "Davom ettirish",
                      style: TextStyle(
                          fontFamily: 'Inter',
                          fontWeight: FontWeight.w500,
                          fontSize: 20,
                          color: Color(0xffffffff)),
                      textAlign: TextAlign.center,
                    ))),
                onTap: () {
                  if (currentIndex == contents.length - 1) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => PageControl()));
                  }
                  _controller.nextPage(
                      duration: const Duration(milliseconds: 130),
                      curve: Curves.bounceIn);
                }),
          ),
        ],
      ),
    );
  }
}
